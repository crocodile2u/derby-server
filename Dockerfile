FROM lpicanco/java11-alpine

RUN cd / && \
    wget http://apache.cs.uu.nl/db/derby/db-derby-10.15.1.3/db-derby-10.15.1.3-bin.tar.gz -O - | tar -xz && \
    mv db-* derby

RUN mkdir /dbs
VOLUME /dbs

WORKDIR /dbs
EXPOSE 1527

ENTRYPOINT ["java", "-jar", "/derby/lib/derbyrun.jar", "server", "start"]
